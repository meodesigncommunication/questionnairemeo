<?php
/**
 * Created by PhpStorm.
 * User: MEO
 * Date: 22.03.2017
 * Time: 15:52
 */

require_once 'library/fpdf/fpdf.php';

# define constant, serialize array
define('CF7DB', 'wp_cf7dbplugin_submits');
define ("QUESTIONS", serialize (array (
    "Les membres de l'équipe ont/avaient les compétences et l'expertise requise.",
    "Les membres de l'équipe ont/avaient les compétences et l'expertise requise.",
    "Nous avons utilisé un langage accessible et sans jargon.",
    "Nous avons compris ce que vous vouliez vraiment.",
    "Nous avons montré notre enthousiasme et notre engagement.",
    "Nous avons démontré des compétences de manière constante et efficace tout au long du mandat.",
    "Nous avons exprimé notre opinion et nos désaccords avec tact.",
    "Nous avons réussi à établir une relation forte et productive avec vous et/ou les membres de votre équipe.",
    "Nous avons compris et répondu à vos attentes.",
    "Nous avons montré de la compréhension pour votre business et les difficultés inhérentes à votre industrie.",
    "Nous vous avons fourni des recommandations pratiques de façon proactive.						",
    "Nous avons travaillé avec vous pour trouver des solutions efficaces en fonction de vos priorités.",
    "Nous avons communiqué de façon efficace les progrès et avancements du travail accompli.",
    "Nous avons atteint les objectifs et tenu les délais.",
    "Nous avons répondu aux demandes en temps utiles.",
    "Nous nous sommes focalisés sur les priorités, buts et objectifs critiques.",
    "Comment jugez-vous en général les prestations de MEO ?",
    "Comment évaluez-vous la qualité et l'originalité des concepts proposés ?",
    "Comment jugez-vous la qualité de la réalisation des concepts en graphisme ?	",
    "Êtes-vous satisfait de l'« account management » ?",
    "Quel est le degré de probabilité que vous recommandiez MEO à un contact et/ou collègue ?",
    "Qu'appréciez-vous le plus chez nous par rapport aux autres agences ?",
    "Dans quels autres domaines pourrions-nous vous aider ?",
    "Comment pouvons-nous maintenir/développer notre relation dans le futur ?"
)));

function generatePDFReport()
{

}

function calculAverageQuestionScore($forms)
{
    $count = 1;
    $tabAverage = array();
    $countForm = count($forms);
    $questions = count(unserialize(QUESTIONS));

    foreach($forms as $form){
        foreach($form as $key => $value){
            for( $i=1 ; $i <= $questions ; $i+=1 )
            {
                if($key == "question-$i"){
                    if(!isset($tabAverage[$key]) || empty($tabAverage[$key]))
                    {
                        $tabAverage[$key] = $value;
                    }else{
                        $tabAverage[$key] += $value;
                    }
                }
            }
        }
        $count = 1;
    }

    foreach($tabAverage as $key => $average){
        $tabAverage[$key] = round($average/$countForm);
    }

    return $tabAverage;
}