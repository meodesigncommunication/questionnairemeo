<p class="txt-info">
    In order to evaluate and improve the services we deliver to you, we would very much llike to find out how you rate differtent aspects of our work for you.
</p>
<hr/>
<div class="form-group">
    <label>Company <span class="required">*</span></label>
    [text* compagny class:form-control]
</div>
<div class="form-group">
    <label>Name of contact <span class="required">*</span></label>
    [text* full_name class:form-control]
</div>
<div class="form-group">
    <label>Function <span class="required">*</span></label>
    [text* function class:form-control]
</div>
<div class="form-group">
    <label>Principal MEO contact <span class="required">*</span></label>
    [select* contact_in class:form-control "Louis Paschoud" "Graham Smith" "Corinne Bersier" "Guenaelle Stulz" "John Echenard" "Fanny Poncet" "Pauline Rochat" "Kyle Mobilia" "Nadia montasri"]
</div>
<p>
<table id="data-grid">
    <tr>
        <td>
            1. Deeply unsatisfactory
        </td>
        <td>
            2. Unsatisfactory
        </td>
        <td>
            3. Not really satisfactory
        </td>
        <td>
            4. Satisfactory
        </td>
        <td>
            5. Fantastic
        </td>
        <td>
            6. Not applicable
        </td>
    </tr>
</table>
</p>

<div class="bloc-question">
    <h2>The team</h2>
    <div>
        <div class="form-group">
            <label>Our team members have the necessary skills and expertise.</label>[radio question-1 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We asked you the right questions.</label>[radio question-2 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We used accessible, jargon-free language.</label>[radio question-3 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We really understood what you wanted.</label>[radio question-4 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We showed enthusiasm for your project and commitment to the outcomes.</label>[radio question-5 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We were efficient capable during the whole project.</label>[radio question-6 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We expressed our opinions, including disagreements, with tact.</label>[radio question-7 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We created a strong relationship based on mutual trust with you and members of your team.</label>[radio question-8 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
    </div>
</div>
<br/>
<div class="bloc-question">
    <h2>The project</h2>
    <div>
        <div class="form-group">
            <label>We understood and met your expectations for the project.</label>[radio question-9 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We showed understanding of your business needs and the particular challenges of your sector.</label>[radio question-10 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We were proactive in making practical recommendations.</label>[radio question-11 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We worked with you to find solutions that met your priorities.</label>[radio question-12 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We efficiently communicated progress, including any delays,  throughout the project lifespan.</label>[radio question-13 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We achieved the objectives set within the timescales agreed.</label>[radio question-14 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We responded to requests in reasonable time.</label>[radio question-15 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>We focussed on critical priorities, goals and objectives.</label>[radio question-16 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
    </div>
</div>

<h2 class="title-section">On a scale of 1 to 10 :</h2>
<div class="form-group">
    <label>How would you rate MEO’s services overall?</label>[select question-17 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
</div>
<hr/>
<div class="form-group">
    <label>How would you rate the quality and originality of our proposals ?</label>[select question-18 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
    <p>Comments :</p>
    [textarea question-detail-18 class:form-control]
</div>
<hr/>
<div class="form-group">
    <label>How would you rate the quality of the design work we provided ?</label>[select question-19 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
    <p>Comments :</p>
    [textarea question-detail-19 class:form-control]
</div>
<hr/>
<div class="form-group">
    <label>How would you rate our account management ?</label>[select question-20 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
    <p>Comments :</p>
    [textarea question-detail-20 class:form-control]
</div>
<hr/>
<div class="form-group">
    <label>How likely are you to recommend MEO to a colleague or contact ?</label>[select question-21 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
    <p>Comments :</p>
    [textarea question-detail-21 class:form-control]
</div>
<h2 class="title-section">Open questions :</h2>
<div class="form-group">
    <label>What do you consider our strengths to be compared to other agencies ?</label>
    [textarea question-detail-22 class:form-control]
</div>
<div class="form-group">
    <label>Could we help you in other areas of your business activities ?</label>
    [textarea question-detail-23 class:form-control]
</div>
<div class="form-group">
    <label>What could we do to strengthen our relationship in the future ?</label>
    [textarea question-detail-24 class:form-control]
</div>
<br/>
[quiz quiz-253 "2+2=|4" "2+8=|10" "6-3=|3" "2-1=|1"]
<br/>
[submit class:btn-meo id:send_form "Send"]