<p class="txt-info">
    Afin d’évaluer et d’améliorer la qualité de nos services, nous souhaitons avoir votre avis sur les services que nous vous avons fournis.
</p>
<hr/>
<div class="form-group">
    <label>Nom de la société <span class="required">*</span></label>
    [text* compagny class:form-control]
</div>
<div class="form-group">
    <label>Nom du client/Contact <span class="required">*</span></label>
    [text* full_name class:form-control]
</div>
<div class="form-group">
    <label>Fonction du client/Contact <span class="required">*</span></label>
    [text* function class:form-control]
</div>
<div class="form-group">
    <label>Contact MEO <span class="required">*</span></label>
    [select* contact_in class:form-control "Louis Paschoud" "Graham Smith" "Corinne Bersier" "Guenaelle Stulz" "John Echenard" "Fanny Poncet" "Pauline Rochat" "Kyle Mobilia" "Nadia montasri"]
</div>
<p>
<table id="data-grid">
    <tr>
        <td>
            1. Très insatisfait
        </td>
        <td>
            2. Insatisfait
        </td>
        <td>
            3. Pas vraiment satisfait
        </td>
        <td>
            4. Satisfait
        </td>
        <td>
            5. Très Satisfait
        </td>
        <td>
            6. Ne sait pas ou n.a.
        </td>
    </tr>
</table>
</p>

<div class="bloc-question">
    <h2>Équipe</h2>
    <div>
        <div class="form-group">
            <label>Les membres de l’équipe ont/avaient les compétences et l’expertise requise.</label>[radio question-1 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous vous avons posé les bonnes questions.</label>[radio question-2 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons utilisé un langage accessible et sans jargon.</label>[radio question-3 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons compris ce que vous vouliez vraiment.</label>[radio question-4 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons montré notre enthousiasme et notre engagement.</label>[radio question-5 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons démontré des compétences de manière constante et efficace tout au long du mandat.</label>[radio question-6 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons exprimé notre opinion et nos désaccords avec tact.</label>[radio question-7 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons réussi à établir une relation forte et productive avec vous et/ou les membres de votre équipe.</label>[radio question-8 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
    </div>
</div>
<br/>
<div class="bloc-question">
    <h2>Mandat</h2>
    <div>
        <div class="form-group">
            <label>Nous avons compris et répondu à vos attentes.</label>[radio question-9 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons montré de la compréhension pour votre business et les difficultés inhérentes à votre industrie.</label>[radio question-10 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous vous avons fourni des recommandations pratiques de façon proactive.</label>[radio question-11 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons travaillé avec vous pour trouver des solutions efficaces en fonction de vos priorités.</label>[radio question-12 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons communiqué de façon efficace les progrès et avancements du travail accompli.</label>[radio question-13 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons atteint les objectifs et tenu les délais.</label>[radio question-14 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous avons répondu aux demandes en temps utiles.</label>[radio question-15 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
        <hr/>
        <div class="form-group">
            <label>Nous nous sommes focalisés sur les priorités, buts et objectifs critiques.</label>[radio question-16 default:1 "1" "2" "3" "4" "5" "6"]
        </div>
    </div>
</div>
<h2 class="title-section">Sur une échelle de 1 à 10 :</h2>
<div class="form-group">
    <label>Comment jugez-vous en général les prestations de MEO ?</label>[select question-17 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
</div>
<hr/>
<div class="form-group">
    <label>Comment évaluez-vous la qualité et l’originalité des concepts proposés ?</label>[select question-18 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
    <p>Remarque(s):</p>
    [textarea question-detail-18 class:form-control]
</div>
<hr/>
<div class="form-group">
    <label>Comment jugez-vous la qualité de la réalisation des concepts en graphisme ?</label>[select question-19 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
    <p>Remarque(s):</p>
    [textarea question-detail-19 class:form-control]
</div>
<hr/>
<div class="form-group">
    <label>Êtes-vous satisfait de l’« account management » ?</label>[select question-20 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
    <p>Remarque(s):</p>
    [textarea question-detail-20 class:form-control]
</div>
<hr/>
<div class="form-group">
    <label>Quel est le degré de probabilité que vous recommandiez MEO à un collègue et/ou contact ?</label>[select question-21 class:select-score "1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
    <div class="col-end"></div>
    <p>Pourquoi ? Merci de commenter ou de donner des exemples afin de clarifier votre pensée.</p>
    [textarea question-detail-21 class:form-control]
</div>
<h2 class="title-section">Questions à développer :</h2>
<div class="form-group">
    <label>Qu’appréciez-vous le plus chez nous par rapport aux autres agences ?</label>
    [textarea question-detail-22 class:form-control]
</div>
<div class="form-group">
    <label>Dans quels autres domaines pourrions-nous vous aider ?</label>
    [textarea question-detail-23 class:form-control]
</div>
<div class="form-group">
    <label>Comment pouvons-nous maintenir/développer notre relation dans le futur ?</label>
    [textarea question-detail-24 class:form-control]
</div>
<br/>
[quiz quiz-253 "2+2=|4" "2+8=|10" "6-3=|3" "2-1=|1"]
<br/>
[submit class:btn-meo id:send_form "Envoyer"]