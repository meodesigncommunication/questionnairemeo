<?php
/*
Template Name: Single report
*/

global $wpdb;
$count = 1;
$questions = array();
$list_questions = unserialize(QUESTIONS);
$time_report = (isset($_GET['form']) && !empty($_GET['form'])) ? $_GET['form'] : '';

if(isset($time_report) && !empty($time_report))
{
    $query = 'SELECT * FROM '.CF7DB.' WHERE submit_time = '.$time_report;
    $results = $wpdb->get_results($query);

    // Traitement des données
    foreach ($results as $key => $value)
    {
        if($value->field_name == 'full_name'){
            $full_name = $value->field_value;
        }else if($value->field_name == 'function'){
            $function = $value->field_value;
        }else if($value->field_name == 'contact_in'){
            $contact_in = $value->field_value;
        }else if($value->field_name == 'project'){
            $project = $value->field_value;
        }else if($value->field_name == 'compagny'){
            $compagny= $value->field_value;
        }else if(strstr($value->field_name, 'question')){
            $questions[$value->field_name] = $value->field_value;
        }
    }

    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','',16);
    $pdf->SetDrawColor(100, 100, 100);

    $x = (210-25)/2;
    $pdf->Image(get_stylesheet_directory().'/images/Logo_dark.png',$x,null,25,22);

    $y = $pdf->GetY();
    $x = $pdf->GetX();

    $pdf->SetY($y+5);
    $pdf->SetFontSize(20);
    $pdf->SetTextColor(221,51,51);
    $pdf->Cell(0,0,'Rapport de satisfaction',0,2,'C');

    $y = $pdf->GetY();
    $pdf->SetY($y+20);
    $pdf->SetFontSize(10);
    $pdf->SetTextColor(10,10,10);
    $pdf->Cell(0,0,'No projet : '.$project,0,2,'C');

    $y = $pdf->GetY();
    $pdf->SetY($y+5);
    $pdf->Cell(0,0,'Nom complet : '.$full_name,0,2,'C');

    $y = $pdf->GetY();
    $pdf->SetY($y+5);
    $pdf->Cell(0,0,'Fonction : '.$function,0,2,'C');

    $y = $pdf->GetY();
    $pdf->SetY($y+5);
    $pdf->Cell(0,0,'Societe : '.$compagny,0,2,'C');

    $y = $pdf->GetY();
    $pdf->SetY($y+5);
    $pdf->Cell(0,0,'Contact MEO : '.$contact_in,0,2,'C');

    $pdf->SetTextColor(100,100,100);

    $y = $pdf->GetY();
    $pdf->SetY($y+10);

    $pdf->SetFontSize(9);
    /*$pdf->cell(10,5,'NO',1,0,'C');
    $pdf->cell(160,5,'QUESTIONS',1,0,'C');
    $pdf->cell(0,5,'NOTES',1,0,'C');

    $y = $pdf->GetY();
    $pdf->SetY($y+5);*/

    foreach($list_questions as $key => $value)
    {
        if($count == 1)
        {
            $y = $pdf->GetY();
            $pdf->SetY($y+8);
            $pdf->SetTextColor(221,51,51);
            $pdf->SetFont('Arial','B',9);
            $pdf->cell(0,8,'Equipe',1,0,'L');
            $pdf->SetFont('Arial','',9);
            $y = $pdf->GetY();
            $pdf->SetY($y+8);
            $pdf->SetTextColor(100,100,100);
        }
        if($count == 9)
        {
            $y = $pdf->GetY();
            $pdf->SetY($y+8);
            $pdf->SetTextColor(221,51,51);
            $pdf->SetFont('Arial','B',9);
            $pdf->cell(0,8,'Mandat',1,0,'L');
            $pdf->SetFont('Arial','',9);
            $y = $pdf->GetY();
            $pdf->SetY($y+8);
            $pdf->SetTextColor(100,100,100);
        }
        if($count == 16)
        {
            $y = $pdf->GetY();
            $pdf->SetY($y+8);
            $pdf->SetTextColor(221,51,51);
            $pdf->SetFont('Arial','B',9);
            $pdf->cell(0,8,'Note sur 10',1,0,'L');
            $pdf->SetFont('Arial','',9);
            $y = $pdf->GetY();
            $pdf->SetY($y+8);
            $pdf->SetTextColor(100,100,100);
        }
        if($count == 21)
        {
            $y = $pdf->GetY();
            $pdf->SetY($y+8);
            $pdf->SetTextColor(221,51,51);
            $pdf->SetFont('Arial','B',9);
            $pdf->cell(0,8,'Questions ouverte',1,0,'L');
            $pdf->SetFont('Arial','',9);
            $y = $pdf->GetY();
            $pdf->SetY($y+8);
            $pdf->SetTextColor(100,100,100);
        }

        $pdf->Cell(8,5,$count,1,0,'C');
        $pdf->Cell(170,5,utf8_decode($value),1,0);
        $pdf->SetTextColor(10,10,10);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(0,5,$questions['question-'.$count],1,0,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(100,100,100);

        if(isset($questions['question-detail-'.$count]) && !empty($questions['question-detail-'.$count]))
        {
            $y = $pdf->GetY();
            $pdf->SetY($y+8);
            $pdf->Cell(19,5,'Remarque :',0,0,'C');

            $y = $pdf->GetY();
            $pdf->SetY($y+5);
            $pdf->SetTextColor(10,10,10);
            $pdf->SetFont('Arial','B',9);
            $pdf->MultiCell(0, 5, $questions['question-detail-'.$count]);
            $pdf->SetFont('Arial','',9);
            $pdf->SetTextColor(100,100,100);
        }

        $count++;

        $y = $pdf->GetY();
        $pdf->SetY($y+5);
    }
    $pdf->Output();

}
