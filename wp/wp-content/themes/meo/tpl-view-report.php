<?php
/*
Template Name: Report Formulaire
*/

global $post, $mk_options, $wpdb;

$count = 1;
$temp_submit_time = null;
$temp_form_name = null;
$array_form = array();
$questions = unserialize(QUESTIONS);

$query = "SELECT * FROM wp_cf7dbplugin_submits";
$results = $wpdb->get_results($query);

foreach($results as $key => $value)
{
    $array_form[$value->submit_time]['form_name'] = $value->form_name;
    $array_form[$value->submit_time][$value->field_name] = $value->field_value;
}
$list_average = calculAverageQuestionScore($array_form);

/*echo '<pre>';
print_r($array_form);
echo '</pre>';

echo '<pre>';
print_r($list_average);
echo '</pre>';*/

?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php bloginfo('description'); ?>">
        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
        <?php wp_head(); ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet" />
        <script>
            // conditionizr.com
            // configure environment tests
            conditionizr.config({
                assets: '<?php echo get_template_directory_uri(); ?>',
                tests: {}
            });
        </script>
    </head>
    <body>
        <section>
            <div id="logo">
                <img src="http://www.meomeo.ch/wp/wp-content/uploads/2014/09/Logo_dark.png" alt="log" />
                <h1><?php echo $post->post_title ?></h1>
            </div>

            <div id="average-score">
                <section class="bloc-chart">
                    <h2>
                        Moyenne des notes
                    </h2>
                    <i class="fa fa-question-circle-o first" aria-hidden="true"></i>
                    <canvas id="scoreOn6" height="100"></canvas>
                    <i class="fa fa-question-circle-o second" aria-hidden="true"></i>
                    <canvas id="scoreOn10" height="100"></canvas>
                </section>
            </div>

            <div id="list-form">
                <h2>
                    Formulaire client
                </h2>
                <?php
                    foreach($array_form as $key => $form)
                    {
                        echo '<div data-popup="'.$key.'" class="form-user" title="Afficher le questionnaire de satisfaction">';
                        echo    '<i class="fa fa-user-circle-o" aria-hidden="true"></i>';
                        echo    '<span class="name">'.$form['full_name'].'&nbsp-&nbsp;'.$form['compagny'].' / n°projet '.$form['project'].'</span>';
                        echo '</div>';
                    }
                ?>
            </div>

        </section>
        <div class="popup-data">
            <div class="list-questions list-questions-first">
                <i class="fa fa-times fa-times-first" aria-hidden="true"></i>
                <?php
                $count = 1;
                foreach($questions as $question)
                {
                    if($count <= 16){
                        echo '<p>Q'.$count.')&nbsp;'.$question.'</p>';
                    }
                    $count++;
                }
                ?>
            </div>
            <div class="list-questions list-questions-second">
                <i class="fa fa-times fa-times-second" aria-hidden="true"></i>
                <?php
                $count = 1;
                foreach($questions as $question)
                {
                    if($count > 16 && $count <= 21) {
                        echo '<p>Q' . $count . ')&nbsp;' . $question . '</p>';
                    }
                    $count++;
                }
                ?>
            </div>
            <?php
            $count = 1;
            foreach($array_form as $key => $form): ?>

                <div data-popup="<?php echo $key ?>" class="form-user-popup">
                    <i class="fa fa-times close-popup" aria-hidden="true"></i>
                    <div class="info-contact">
                        <div class="icon-contact">
                            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                        </div>
                        <div class="info-details">
                            <p><?php echo $form['full_name'] . ' - ' . $form['function']; ?></p>
                            <p><?php echo $form['compagny'].' / n°projet '.$form['project']; ?></p>
                        </div>
                        <div class="col-end"></div>
                    </div>
                    <hr/>
                    <p><?php echo 'Mon contact meo est : <strong>' . $form['contact_in']; ?></strong></p>
                    <?php $count = 1 ?>
                    <?php foreach($questions as $question): ?>
                        <hr/>
                        <div class="question">
                            <p>
                                <?php echo 'Q'.$count.')&nbsp;'.$question.'<span class="reponse">'.$form['question-'.$count].'</span>'; ?>
                                <?php
                                if($count > 17){
                                    echo '<br/>Remarques:<br/><strong>';
                                    echo (!empty($form['question-detail-'.$count])) ? $form['question-detail-'.$count] : 'Pas de remarque';
                                    echo '</strong>';
                                }
                                ?>
                            </p>
                        </div>
                        <?php $count++; ?>
                    <?php endforeach; ?>
                    <div class="col-end"></div>
                    <a target="_blank" href="<?php echo site_url('report-single-pdf'); ?>?form=<?php echo $key ?>">
                        <input class="btn-generate-pdf" type="button" value="Imprimer" />
                    </a>
                    <input type="button" class="btn-close-popup" value="Fermer" />
                </div>

            <?php endforeach; ?>
        </div>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/Chart.bundle.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/chart.js" type="text/javascript"></script>
        <script>

            window.$ = jQuery;

            $(document).ready(function(){

                // Popup question action
                $('.fa.fa-question-circle-o.first').click(function(){
                    $('.popup-data').css('display','block');
                    $('.list-questions-first').css('display','block');
                });

                $('.fa.fa-question-circle-o.second').click(function(){
                    $('.popup-data').css('display','block');
                    $('.list-questions-second').css('display','block');
                });

                $('.fa-times-first').click(function(){
                    $('.popup-data').css('display','none');
                    $('.list-questions-first').css('display','none');
                });

                $('.fa-times-second').click(function(){
                    $('.popup-data').css('display','none');
                    $('.list-questions-second').css('display','none');
                });

                $('.form-user').click(function(){
                    var id_form = $(this).attr('data-popup');
                    $('.popup-data').css('display','block');
                    $('.form-user-popup[data-popup="'+id_form+'"]').css('display','block');
                });

                $('.fa-times.close-popup').click(function(){
                    $('.popup-data').css('display','none');
                    $(this).parent().css('display','none');
                });

                $('.btn-close-popup').click(function(){
                    $('.popup-data').css('display','none');
                    $(this).parent().css('display','none');
                });

                var count = 1;
                var allScoreAverage = [];
                var backgroundColor = [];
                var borderColor = [];
                var scoreAverageJSON = '<?php echo json_encode($list_average); ?>'
                allScoreAverage = JSON.parse(scoreAverageJSON);
                // FIRST PART FORM FR
                var data = [];
                $.each(allScoreAverage, function(i, item){
                    if(count <= 16){
                        data.push(allScoreAverage['question-'+count]);
                        backgroundColor.push('rgba(50,50,50,0.3)');
                        borderColor.push('rgba(50,50,50,0.5)');
                    }
                    count += 1;
                });
                data.push(6);
                backgroundColor.push('rgba(248, 57, 57, 0)');
                borderColor.push('rgba(221,51,51,0)');
                var ctx = document.getElementById("scoreOn6");
                var chartFirstFR = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["Q1","Q2","Q3","Q4","Q5","Q6","Q7","Q8","Q9","Q10","Q11","Q12","Q13","Q14","Q15","Q16",""],
                        datasets: [{
                            label: 'Note sur 6',
                            data: data,
                            backgroundColor: backgroundColor,
                            borderColor: borderColor,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
                // SECOND PART FORM FR
                count = 1;
                var data = [];
                var backgroundColor = [];
                var borderColor = [];
                $.each(allScoreAverage, function(i, item){
                    if(count > 16){
                        data.push(allScoreAverage['question-'+count]);
                        backgroundColor.push('rgba(50,50,50,0.3)');
                        borderColor.push('rgba(50,50,50,0.5)');
                    }
                    count += 1;
                });
                data.push(10);
                backgroundColor.push('rgba(248, 57, 57, 0)');
                borderColor.push('rgba(221,51,51,0)');
                var ctx = document.getElementById("scoreOn10");
                var chartSecondFR = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["Q17","Q18","Q19","Q20","Q21",""],
                        datasets: [{
                            label: 'Note sur 10',
                            data: data,
                            backgroundColor: backgroundColor,
                            borderColor: borderColor,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            });

        </script>

    </body>
</html>
