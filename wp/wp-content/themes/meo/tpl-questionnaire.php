<?php
/*
Template Name: Questionnaire
*/

global $post, $mk_options;

$project = (isset($_GET['project']) && !empty($_GET['project'])) ? $_GET['project'] : '0000';

?>

<html>
    <head>

        <meta charset="<?php bloginfo('charset'); ?>">

        <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php bloginfo('description'); ?>">

        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
        <?php wp_head(); ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

        <link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet" />
        <script>
            // conditionizr.com
            // configure environment tests
            conditionizr.config({
                assets: '<?php echo get_template_directory_uri(); ?>',
                tests: {}
            });
        </script>
    </head>
    <body>
        <section>
            <div id="logo">
                <img src="http://www.meomeo.ch/wp/wp-content/uploads/2014/09/Logo_dark.png" alt="log" />
                <h1><?php echo $post->post_title ?></h1>
            </div>
            <div class="formulaire">
                <?php echo do_shortcode($post->post_content); ?>
            </div>
        </section>
        <script>
            window.$ = jQuery;

            $(document).ready(function(){

                $('#project-number').val('<?php echo $project ?>');

            });

            function getRedirectToSendForm()
            {
                alert('ALERT');
            }
        </script>
    </body>
</html>