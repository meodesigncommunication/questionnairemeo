<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'rml_questionnaire');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'rml_question');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '1658meopass');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'rml.myd.sharedbox.com');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'jKU:*Aj`IxB[l`ITbX^OP*#JuIG-o>=;jSemj{4WW%%J<)29B_1*5/:I!kV$[lwE');
define('SECURE_AUTH_KEY',  '<ac*3Pf&Z0K&,BXF&:-^pEf6|d|Sbj8nqCko)XeeMRWdQ;!CC4EQ* y0>/P`U2nx');
define('LOGGED_IN_KEY',    'H[,{lT,`7!Y3U{P9a``%Ft~Jae)TEx~R_=_m,!(k`5w:@JK|#y/rOm:wq_!):]jP');
define('NONCE_KEY',        ' ys5Bs%506{@M8{jr[?j[cKoNROGzHWrf9[9-qna6,)co@79U_|5D=7bT0J0N:C[');
define('AUTH_SALT',        'A~2Z3iZ!}Hi;2M6?HIW}Q35g|OXACt,FqHY*Toj1^3[E*m6*q*tc%k!>e#!n9c1M');
define('SECURE_AUTH_SALT', 'hdDN5FJ,L}<]?4G~K(%{KZ*Z@gL=W0Z[2L#Ww{/:jiaWIdV!w/T*,frU H~J^1AU');
define('LOGGED_IN_SALT',   't<s~IOw@4[lW_sLqxM)Pb{37)kL^(/!uHpdpm:F}Y3x3&/GHxDbgRfdVRZm(;z?r');
define('NONCE_SALT',       'ewO0Ao!tfl:a[]g4mB9ecPoIdaL!5X81;_@HTQu2t8[BT^]3#GXQLBh}qm3}bbM:');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');